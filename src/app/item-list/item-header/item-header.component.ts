import { Item } from "./../item-list.interface";
import { Component, EventEmitter, Input, Output } from "@angular/core";
import { CommonModule } from "@angular/common";

@Component({
  selector: "his-item-header",
  standalone: true,
  imports: [CommonModule],
  templateUrl: "./item-header.component.html",
  styleUrls: ["./item-header.component.scss"],
})
export class ItemHeaderComponent {
  @Input() item!: Item;

  @Output() delete: EventEmitter<Item> = new EventEmitter<Item>();
  @Output() insert: EventEmitter<Item> = new EventEmitter<Item>();
  @Output() update: EventEmitter<Item> = new EventEmitter<Item>();

  /**
   * 觸發刪除事件
   * @param item
   */
  doDeleteItem(item: Item) {
    this.delete.emit(item);
  }

  /**
   * 觸發修改事件
   * @param item
   */
  doUpdateItem(item: Item) {
    this.update.emit(item);
  }

  /**
   * 觸發新增事件
   * @param item
   */
  doInsertItem(item: Item) {
    this.insert.emit(item);
  }
}
